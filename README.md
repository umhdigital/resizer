# README #

This README would normally document whatever steps are necessary to get your application up and running.

Some default values could be configured via config.toml

### What is this repository for? ###

The GOLANG server to resize images

### Usage example ###

http://resizer.lena.ivanezko.com:9090/crop?w=140&h=105&url=http://bm.img.com.ua/berlin/storage/news/orig/./20/b20c455749749f9b7939fb23d0067f20.jpeg

### Parameters ###

w - target width (0-1500)

h - target height (0-1500)

sx - source x for crop (0-5000)

sy - source y for crop (0-5000)

sw - source width for crop (0-5000)

sh - source height for crop (0-5000)

nx - non-breakable x (0-5000)

ny - non-breakable y  (0-5000)

nw - non-breakable width  (0-5000)

nh - non-breakable height (0-5000)

url - url of the original image

q - jpeg quality (0-100) default 90

m - resample method (1-30, default-22)

### Non-breakable area ###

If we have an area which should'n be cut by crop if possible (fjr example - human face), 
we can set it and crop will avoid breaking this area if possible

### Logic ###

Lets say, we have an original image with sizes 600x400

Then request with params:

1) w=300&h=300 will crop the maximum centered image from orig (400x400), then resize the result to 300x300

2) w=300 will resize original image to with 300 (300x200)

3) h=300 will resize original image to height 300 (450x300)

4) w=300&h=300&sx=10&sy=20&sw=400&sh=200 will crop orig by 400x200x10x20 (400x200) then crop to target ratio 200x200 then resize to 300x300

5) w=300&sx=10&sy=20&sw=400&sh=200 will crop orig by 400x200x10x20 (400x200) then restore height by crop ratio (400/200~300/150) and resize to 300x150

6) w=300&h=600&sx=10&sy=20&sw=400&sh=200 will crop orig by 400x200x10x20 (400x200) then crop centered 100x200x150x0 then resize to 300x600

### Errors ###

If Resizer cannot load orig image, it uses (config)error.defaultOrig and resizes it to target size

in case of error client redirects to (config)error.redirect

the reason of error prints to special header (config)error.printErrorInHeader 

Syslog fills anyway

### Output ###

Jpeg only

header for Nginx:

X-Accel-Expires: 604800

### Protection ###

You can set the regexp for your domains only in (config)protection.allowedOrigDomains

All the requests with origs which are not math the regular expression will be redirected to default image

### Run ###

docker run -d -p 9090:9090 ivanezko/resizer

There is no any cache for output, so use Nginx as frontend cache


### Debug ###

docker build -t golang-resizer .

docker run -it --rm -p 9090:9090 --name golang-resizer-running golang-resizer

### Resample methods ###
		1: imagick.FILTER_POINT
		2: imagick.FILTER_BOX
		3: imagick.FILTER_TRIANGLE
		4: imagick.FILTER_HERMITE
		5: imagick.FILTER_HANNING
		6: imagick.FILTER_HAMMING
		7: imagick.FILTER_BLACKMAN
		8: imagick.FILTER_GAUSSIAN
		9: imagick.FILTER_QUADRATIC
		10: imagick.FILTER_CUBIC
		11: imagick.FILTER_CATROM
		12: imagick.FILTER_MITCHELL
		13: imagick.FILTER_JINC
		14: imagick.FILTER_SINC
		15: imagick.FILTER_SINC_FAST
		16: imagick.FILTER_KAISER
		17: imagick.FILTER_WELSH
		18: imagick.FILTER_PARZEN
		19: imagick.FILTER_BOHMAN
		20: imagick.FILTER_BARTLETT
		21: imagick.FILTER_LAGRANGE
		22: imagick.FILTER_LANCZOS
		23: imagick.FILTER_LANCZOS_SHARP
		24: imagick.FILTER_LANCZOS2
		25: imagick.FILTER_LANCZOS2_SHARP
		26: imagick.FILTER_ROBIDOUX
		27: imagick.FILTER_ROBIDOUX_SHARP
		28: imagick.FILTER_COSINE
		29: imagick.FILTER_SPLINE
		30: imagick.FILTER_SENTINEL
		
### Changelog ###
1.0.3 - added nonbreakable area

1.0.4 - fix 404 caching

