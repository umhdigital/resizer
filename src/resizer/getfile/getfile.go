package getfile

import (
	"github.com/spf13/viper"
	"fmt"
	"net/http"
	"github.com/golang/groupcache"
	"io/ioutil"
	"errors"
)


var cacher *groupcache.Group
var StatHttp int64
var StatTotal int64

func init() {
	StatHttp = 0
	StatTotal = 0
}

func Stat(name string) int64 {
	switch name {
	case "total": return StatTotal
	case "http": return StatHttp
	}
	return 0
}

func initCacher() {
	mem := int64(viper.GetInt("cacher.memory"))
	fmt.Println("init cacher")
	cacher = groupcache.NewGroup("cacher", mem<<20, groupcache.GetterFunc(getImage))
}

func LoadImage(url string, resultBuffer *[]byte) error {
	StatTotal++
	if (cacher == nil) {
		initCacher()
	}
	//var buf []byte
	err := cacher.Get(nil, url, groupcache.AllocatingByteSliceSink(resultBuffer))
	if err != nil {
		fmt.Println("Get error in cacher.Get:", err)
		return err
	}

	return nil
}

func getImage(ctx groupcache.Context, key string, dest groupcache.Sink) error {
	StatHttp++
	fmt.Println("Not in cache, load ", key)
	if (key[0:2] == "//") { // fix universal url prefix
		key = fmt.Sprintf("http:%s", key)
	}
	resp, err := http.Get(key)
	if err != nil {
		fmt.Println("getImage Http get error:", err)
		return err
	}
	defer resp.Body.Close()

	if (resp.StatusCode != 200) {
		t := fmt.Sprintf("Cant load original: %s", key)
		return errors.New(t)
	}

	imgData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("getImage read body error:", err)
		return err
	}
	dest.SetBytes(imgData)
	return nil
}


