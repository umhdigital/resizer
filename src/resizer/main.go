package main

import (
	"github.com/spf13/viper"
	"log"
	"runtime"
	"fmt"
	"net/http"
	"resizer/resizer"
)

func init() {

}

func main() {
	numCPU := runtime.NumCPU()
	fmt.Printf("Available CPU's: %d\n", numCPU)

	fmt.Println("Reading config...")
	viper.SetConfigName("config")
	viper.AddConfigPath("/go")
	viper.SetConfigType("toml")
	err1 := viper.ReadInConfig()
	if err1 != nil { // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s \n", err1))
	}

	server := viper.GetString("server.host")+":"+viper.GetString("server.port")
	fmt.Println("Starting server on "+server)

	http.HandleFunc("/crop", resizer.CropImage)
	http.HandleFunc("/stat", resizer.Stat)


	/*server := &http.Server{
		Addr:           ":8080",
		Handler:        myHandler,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	log.Fatal(s.ListenAndServe())
	*/
	err := http.ListenAndServe(server, nil)
	if err != nil {
		log.Fatal("start error: ", err)
	}
}
